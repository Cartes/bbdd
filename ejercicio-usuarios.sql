DROP USER IF EXISTS 'administrador'@'localhost';
CREATE USER 'administrador'@'localhost' IDENTIFIED BY 'admin12';
GRANT ALL ON *.* TO 'administrador'@'localhost'; || GRANT ALL PRIVILEGES ON *.* TO 'administrador'@'localhost';
/*GRANT UPDATE, SELECT, DELETE, INSERT ON Opers.* TO 'administrador'@'localhost'; PARA GERENTE */


DROP USER IF EXISTS 'cliente'@'localhost';
CREATE USER 'cliente'@'localhost' IDENTIFIED BY 'cliente123';
GRANT UPDATE(Nombre), SELECT ON Opers.CLIENTES TO 'cliente'@'localhost';
GRANT SELECT ON Opers.PRODUCTOS TO 'cliente'@'localhost';
GRANT SELECT ON Opers.* TO 'cliente'@'localhost';


DROP USER IF EXISTS 'gerente'@'localhost';
CREATE USER 'gerente'@'localhost' IDENTIFIED BY 'melon';
GRANT UPDATE, SELECT, INSERT ON Opers.* TO 'gerente'@'localhost';


