DROP DATABASE IF EXISTS Nicolas;
CREATE DATABASE Nicolas;

USE Nicolas;

CREATE TABLE CONTACTO (
        ID TINYINT AUTO_INCREMENT PRIMARY KEY,
        Nombre VARCHAR(20),
        Apellidos VARCHAR(30)
);

DESC CONTACTO;

CREATE TABLE REALEZA (
        IDCONTACTO TINYINT,
        Ocupacion ENUM('Rey', 'Principe', 'Infanta', 'Otros'),
        FOREIGN KEY (IDCONTACTO) REFERENCES CONTACTO(ID)
);

DESC REALEZA;

CREATE TABLE POLITICO (
        IDCONTACTO TINYINT,
        Rango TINYINT UNSIGNED,
        Partido ENUM ('PP', 'PSOE', 'VOX', 'C´S', 'PODEMOS'),
        FOREIGN KEY (IDCONTACTO) REFERENCES CONTACTO(ID)
);

DESC POLITICO;

CREATE TABLE FAMOSO (
        IDCONTACTO TINYINT,
        Numero TINYINT UNSIGNED,
        Apodo VARCHAR(20),
        FOREIGN KEY (IDCONTACTO) REFERENCES CONTACTO(ID)
);

DESC FAMOSO;

CREATE TABLE EVENTO (
        Lugar VARCHAR(20),
        hora TIME,
        Fecha DATE,
        ID_CONTACTO_FAMOSO TINYINT,
        Cargo VARCHAR(20),
        FOREIGN KEY (ID_CONTACTO_FAMOSO) REFERENCES FAMOSO(IDCONTACTO)
);

DESC EVENTO;

CREATE TABLE PRESENTA (
        ID_CONTACTO_PRESENTADOR TINYINT,
        ID_CONTACTO_PRESENTADO TINYINT,
        FOREIGN KEY (ID_CONTACTO_PRESENTADOR) REFERENCES CONTACTO(ID), FOREIGN KEY (ID_CONTACTO_PRESENTADO) REFERENCES CONTACTO(ID)
);

DESC PRESENTA;

