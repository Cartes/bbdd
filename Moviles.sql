
CREATE TABLE CLIENTES (
  DNI char(9) NOT NULL,
  Nombre varchar(20),
  Primer_Apellido varchar(20),
  Segundo_Apellido varchar(20),
  Telefono int(9),
  EMail varchar(50),
  PRIMARY KEY (DNI)
);


CREATE TABLE COMPRAS (
  DNI_CLIENTE varchar(20),
  Nombre_TIENDA varchar(10),
  Marca_MOVILES_LIBRE varchar(10),
  Modelo_MOVIL_LIBRE varchar(20),
  Dia date
);


CREATE TABLE CONTRATOS (
  DNI_CLIENTE varchar(20),
  Nombre_TIENDA varchar(10),
  Nombre_OPERADORAS_TARIFAS_OFERTAS varchar(20),
  Nombre_TARIFAS_OFERTAS varchar(10),
  Marca_MOVILES_OFERTAS varchar(20),
  Modelo_MOVILES_OFERTAS varchar(20),
  Dia date
);


CREATE TABLE MOVILES (
  Marca varchar(20),
  Modelo varchar(10),
  Descripción varchar(10),
  SO varchar(10),
  RAM int(1),
  PulgadasPantalla int(2),
  CamaraMPX int(2),
  PRIMARY KEY (Marca,Modelo)
);


CREATE TABLE MOVIL_CONTRATO (
  Marca_MOVILES varchar(20),
  Modelo_MOVILES varchar(10),
  Nombre_OPERADORAS varchar(10),
  Precio float,
  FOREIGN KEY (Marca_MOVILES, Modelo_MOVILES) REFERENCES MOVILES (Marca, Modelo),
  FOREIGN KEY (Nombre_OPERADORAS) REFERENCES OPERADORAS (Nombre)
);

CREATE TABLE MOVIL_LIBRE(
  Marca_MOVILES varchar(20),
  Modelo_MOVILES varchar(10),
  Precio float,
 FOREIGN KEY (Marca_MOVILES, Modelo_MOVILES) REFERENCES MOVILES (Marca, Modelo)
);

CREATE TABLE OFERTAS(
  Nombre_OPERADORAS_TARIFAS varchar(20),
  Nombre_TARIFAS varchar(10),
  Marca_MOVIL_CONTRATO varchar(10),
  Modelo_MOVIL_CONTRATO varchar(20)
);

CREATE TABLE OPERADORAS(
  Nombre varchar(20),
  ColorLogo varchar(10),
  PorcentajeCobertura int(3),
  FrecuenciaGSM varchar(5),
  PaginaWEB varchar(100),
  PRIMARY KEY (Nombre)
);
CREATE TABLE TARIFAS (
  Nombre varchar(20),
  Nombre_OPERADORAS varchar(10),
  TamanoDatos varchar(10),
  TipoDatos varchar(20),
  MinutosGratis int(10),
  SMSGratis int(10),
  PRIMARY KEY (Nombre),
  FOREIGN KEY (Nombre_OPERADORAS) REFERENCES OPERADORAS(Nombre)
);
CREATE TABLE TIENDAS(
  Nombre varchar(20),
  Provincia varchar(20),
  Localidad varchar(20),
  Direccion varchar(40),
  Telefono int(9),
  DiaApertura varchar(20),
  DiaCierre varchar(20),
  HoraApertura time,
  HoraCierre time,
  PRIMARY KEY (Nombre)
);
