DROP PROCEDURE IF EXISTS ActualizacionLimiteCredito
USE jardineria;

delimiter |

DROP TABLE IF EXISTS ActualizacionLimiteCredito

CREATE PROCEDURE ActualizacionLimiteCredito
     BEGIN
        DECLARE Fecha DATE, CodigoCliente INT, Incremento DECIMAL (5,2);
        DECLARE Incremento = Incremento * 0,15;

        CREATE TABLE ActualizacionLimiteCredito (Fecha DATE, CodigoCliente INT, Incremento DECIMAL (5,2));
        INSERT INTO ActualizacionLimiteCredito VALUES ((SELECT Pagos.FechaPago FROM Pagos));
     END;

     SET Fecha = (SELECT Pagos.FechaPago FROM Pagos);
     SET CodigoCliente = (SELECT Clientes.CodigoCliente,Pagos.FechaPago FROM Clientes JOIN Pagos ON Clientes.CodigoCliente=Pagos.CodigoCliente WHERE Pagos.FechaPago LIKE '2008%' OR Pagos.FechaPago LIKE '2009%' ORDER BY Clientes.CodigoCliente);
     SET Incremento = Incremento * 0,15;





CREATE TABLE ActualizacionLimiteCredito {

}
