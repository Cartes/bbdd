/*

/*Procedimiento*/
/*DROP PROCEDURE IF EXISTS hola;*/
/*
delimiter //

CREATE PROCEDURE hola()
BEGIN
    SELECT ('Hola');
END//

delimiter ;

CALL hola(); /*Llamadas a los procedimientos */

DROP PROCEDURE IF EXISTS p;

DELIMITER |
CREATE PROCEDURE p(IN v INT)
  BEGIN

    WHILE v<13 DO
   CASE v
      WHEN 1 THEN SELECT 'Enero';
      WHEN 2 THEN SELECT 'Febrero';
      WHEN 3 THEN SELECT 'Marzo';
      WHEN 4 THEN SELECT 'Abril';
      WHEN 5 THEN SELECT 'Mayo';
      WHEN 6 THEN SELECT 'Junio';
      WHEN 7 THEN SELECT 'Julio';
      WHEN 8 THEN SELECT 'Agosto';
      WHEN 9 THEN SELECT 'Septiembre';
      WHEN 10 THEN SELECT 'Octubre';
      WHEN 11 THEN SELECT 'Noviembre';
      WHEN 12 THEN SELECT 'Diciembre';
   ELSE
   BEGIN
    SELECT "Introduce un mes de 1 a 12";
   END;
   END CASE;
    SET v = v+1;
END WHILE;
  END;
|


/*
CREATE FUNCTION Name (nombre VARCHAR(20))
    RETURNS VARCHAR(60) DETERMINISTIC /*Tipo de devuelta de dato el RETURNS*/ /*Es obligatorio el returns*/
  /*  RETURN CONCAT('Hello ', nombre,'!'); /*Es el valor que devuelvo*/

/*Select Name(); LLamadas a una funcion*/
