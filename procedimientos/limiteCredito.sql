DROP PROCEDURE IF EXISTS LimiteCredito;
USE jardineria;

delimiter |
/*Creamos el Procedimiento*/
CREATE PROCEDURE LimiteCredito(Cliente INT) /*Declaramos Cliente para que a la hora de llamar al procedimiento, indiquemos al cliente al cual se le va a incrementar en 15%*/
BEGIN
    /*Declaracion de variables*/
    DECLARE Fecha DATE;
    DECLARE CodigoCliente INT;
    DECLARE Credito FLOAT;
    DECLARE Porcentaje FLOAT;
    DECLARE Incremento DECIMAL (15,2);

    /*Creamos la tabla*/
    DROP TABLE IF EXISTS LimiteCredito;

    CREATE TABLE LimiteCredito (Fecha DATE, CodigoCliente INT, Incremento DECIMAL(15,2));

    SET Fecha = (SELECT CURDATE());
    SET CodigoCliente = (SELECT Clientes.CodigoCliente FROM Clientes WHERE Clientes.CodigoCliente = Cliente);
    SET Credito = (SELECT Clientes.LimiteCredito FROM Clientes WHERE Clientes.CodigoCliente = Cliente);
    SET Porcentaje = (Credito * 0.15);
    SET Incremento = Credito + Porcentaje;

    /*Indicamos que se actualize en el campo LimiteCredito, la suma del Incremento en el que se aplica el 15%*/
    UPDATE Clientes SET LimiteCredito = Incremento WHERE CodigoCliente = Cliente;

    /*Insertamos los datos en la tabla*/
    INSERT INTO LimiteCredito VALUES (Fecha, CodigoCliente, Porcentaje);
END;

|

delimiter ;
