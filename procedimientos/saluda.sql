DROP PROCEDURE IF EXISTS saluda;

delimiter //

CREATE PROCEDURE saluda (IN nombre VARCHAR(30))
BEGIN
    SELECT (CONCAT('Hola ', nombre)) AS 'Tu name';
END//

delimiter ;

CALL saluda('Alejandro');

