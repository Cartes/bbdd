DROP PROCEDURE IF EXISTS Probando;
USE jardineria;

delimiter |


DROP TABLE IF EXISTS probando;
/*Creamos el Procedimiento*/
CREATE PROCEDURE Probando(Cliente INT) /*Declaramos Cliente para que a la hora de llamar al procedimiento, indiquemos al cliente al cual se le va a incrementar en 15%*/
BEGIN

    /*Declaracion de variables*/
    DECLARE Fecha DATE;
    DECLARE CodigoCliente INT;
    DECLARE Credito FLOAT;
    DECLARE Porcentaje FLOAT;
    DECLARE Incremento DECIMAL (15,2);

    /*Creamos la tabla*/
    DROP TABLE IF EXISTS probando;

    CREATE TABLE probando (Fecha DATE, CodigoCliente INT, Incremento DECIMAL(15,2));

    SET Fecha = (SELECT CURDATE());
    SET CodigoCliente = (SELECT Clientes.CodigoCliente FROM Clientes WHERE Clientes.CodigoCliente = Cliente);
    SET Credito = (SELECT Clientes.LimiteCredito FROM Clientes WHERE Clientes.CodigoCliente = Cliente);
    SET Porcentaje = (Credito * 0.15);
    SET Incremento = Credito + Porcentaje;

    UPDATE Clientes SET LimiteCredito = Incremento WHERE CodigoCliente = Cliente;

    /*Insertamos los datos en la tabla*/
    INSERT INTO probando VALUES (Fecha, CodigoCliente, Porcentaje);
END;

|

delimiter ;
