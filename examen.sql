DROP DATABASE IF EXISTS ExamenStarkTrek;
CREATE DATABASE ExamenStarkTrek;
USE ExamenStarkTrek;

CREATE TABLE Actores (
    Nombre VARCHAR(20),
    Personaje VARCHAR(30),
    FechaNacimineto DATE,
    Nacionalidad VARCHAR(20),

    PRIMARY KEY(Nombre)
);

CREATE TABLE Personajes (
    Nombre VARCHAR(30),
    Raza VARCHAR(20),
    GraduacionMilitar ENUM('Capitan', 'Teniente', 'Almirante', 'Oficial'),
    NombreActor VARCHAR(20),
    NombrePersonajes VARCHAR(30),

    CONSTRAINT PK_Personajes PRIMARY KEY(Nombre),
    CONSTRAINT FK_Personajes_Actores FOREIGN KEY (NombreActor) REFERENCES Actores(Nombre),
    CONSTRAINT FK_Personajes_Personajes FOREIGN KEY (NombrePersonajes) REFERENCES Personajes(Nombre)
);

CREATE TABLE Naves (
    Nombre VARCHAR(30),
    Codigo INT UNSIGNED,
    NumTripulantes INT UNSIGNED,

    PRIMARY KEY (Codigo)
);

CREATE TABLE Planetas (
    Codigo INT UNSIGNED,
    Nombre VARCHAR(30),
    Galaxia VARCHAR(40),
    Problema VARCHAR(255),
    CodigoNave INT UNSIGNED,

    CONSTRAINT PK_Planetas PRIMARY KEY (Codigo),
    CONSTRAINT FK_Planetes_Naves FOREIGN KEY (CodigoNave) REFERECES Naves(Codigo)
);

CREATE TABLE Capitulos (
    Numero INT UNSIGNED,
    Temporada INT UNSIGNED,
    Titulo VARCHAR(50),
    Orden INT UNSIGNED,
    CodigoPlaneta INT UNSIGNED,

    CONSTRAINT PK_Capitulos (Numero, Temporada),
    CONSTRAINT FK_Capitulos_Planetas FOREIGN KEY (CodigoPlaneta) REFERECES Planetas(Codigo)
);

CREATE TABLE AparicionesSerie (
    NombrePersonaje VARCHAR(30),
    NumeroCapitulo INT UNSIGNED,
    NumeroTemporada INT UNSIGNED,

    CONSTRAINT FK_AparicionesSerie_Personajes FOREIGN KEY (NombrePersonaje) REFERECES Personajes(Nombre),
    CONSTRAINT FK_AparicionesSerie_Capitulos FOREIGN KEY (NumeroCapitulo, NumeroTemporada) REFERECES Capitulos(Numero,Temporada)
);

CREATE TABLE Peliculas (
    Titulo VARCHAR(30),
    AñoLanzamiento YEAR,
    Director VARCHAR(50),

    CONSTRAINT PK_Peliculas PRIMARY KEY (Titulo)
);

CREATE TABLE AparicionesPelis (
    NombrePersonaje VARCHAR(30),
    TituloPelicula VARCHAR(30),
    Protagonista ENUM('Si', 'No'),

    CONSTRAINT FK_AparicionesPelis
)
