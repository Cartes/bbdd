-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: ejercicio4
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DOMINA`
--

DROP TABLE IF EXISTS `DOMINA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DOMINA` (
  `nombre_PERSONAJE_sumiso` varchar(30) DEFAULT NULL,
  `nombre_PERSONAJE_dominante` varchar(30) DEFAULT NULL,
  KEY `nombre_PERSONAJE_sumiso` (`nombre_PERSONAJE_sumiso`),
  KEY `nombre_PERSONAJE_dominante` (`nombre_PERSONAJE_dominante`),
  CONSTRAINT `DOMINA_ibfk_1` FOREIGN KEY (`nombre_PERSONAJE_sumiso`) REFERENCES `PERSONAJE` (`nombre`),
  CONSTRAINT `DOMINA_ibfk_2` FOREIGN KEY (`nombre_PERSONAJE_dominante`) REFERENCES `PERSONAJE` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DOMINA`
--

LOCK TABLES `DOMINA` WRITE;
/*!40000 ALTER TABLE `DOMINA` DISABLE KEYS */;
/*!40000 ALTER TABLE `DOMINA` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ESCENARIO`
--

DROP TABLE IF EXISTS `ESCENARIO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ESCENARIO` (
  `numero` int(4) NOT NULL,
  `tiempo` time DEFAULT NULL,
  `riesgo` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`numero`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ESCENARIO`
--

LOCK TABLES `ESCENARIO` WRITE;
/*!40000 ALTER TABLE `ESCENARIO` DISABLE KEYS */;
/*!40000 ALTER TABLE `ESCENARIO` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OBJETO`
--

DROP TABLE IF EXISTS `OBJETO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OBJETO` (
  `codigo` int(8) unsigned zerofill NOT NULL,
  `nombrePERSONAJE` varchar(30) DEFAULT NULL,
  `numeroESCENARIO` int(4) DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `minuto` time DEFAULT NULL,
  `segundo` time DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `nombrePERSONAJE` (`nombrePERSONAJE`),
  KEY `numeroESCENARIO` (`numeroESCENARIO`),
  CONSTRAINT `OBJETO_ibfk_1` FOREIGN KEY (`nombrePERSONAJE`) REFERENCES `PERSONAJE` (`nombre`),
  CONSTRAINT `OBJETO_ibfk_2` FOREIGN KEY (`numeroESCENARIO`) REFERENCES `ESCENARIO` (`numero`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OBJETO`
--

LOCK TABLES `OBJETO` WRITE;
/*!40000 ALTER TABLE `OBJETO` DISABLE KEYS */;
/*!40000 ALTER TABLE `OBJETO` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PERSONAJE`
--

DROP TABLE IF EXISTS `PERSONAJE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PERSONAJE` (
  `nombre` varchar(30) NOT NULL,
  `fuerza` int(2) DEFAULT NULL,
  `habilidad` int(2) DEFAULT NULL,
  `inteligencia` int(2) DEFAULT NULL,
  `numeroESCENARIO` int(4) DEFAULT NULL,
  PRIMARY KEY (`nombre`),
  KEY `numeroESCENARIO` (`numeroESCENARIO`),
  CONSTRAINT `PERSONAJE_ibfk_1` FOREIGN KEY (`numeroESCENARIO`) REFERENCES `ESCENARIO` (`numero`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PERSONAJE`
--

LOCK TABLES `PERSONAJE` WRITE;
/*!40000 ALTER TABLE `PERSONAJE` DISABLE KEYS */;
/*!40000 ALTER TABLE `PERSONAJE` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-05 10:11:15
