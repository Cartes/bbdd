INSERT INTO PERSONA VALUES
('Walter', 'White', 'Profesor'),
('Jessie', 'Pinkman', 'Dealer'),
('Saul', 'Guzman', 'Abogado');
SELECT * FROM PERSONA;

INSERT INTO OBJETO VALUES
('coche', 'grande',NULL), 
('mochila', 'mediano','coche'),
('Pistola', 'pequeño','mochila');
SELECT * FROM OBJETO;

INSERT INTO SITUACION VALUES
(9, 'caravana', 'Jessie', 'Pinkman', 'Heisenberg', 'Baby blue'),
(10, 'despacho', 'Saul', 'Guzman', 'Traje', 'Pasta $$');
SELECT * FROM SITUACION;

INSERT INTO LLEVA VALUES
(9,'Pistola'),
(10, 'mochila');
SELECT * FROM LLEVA;
